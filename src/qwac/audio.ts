export const enum OscillatorType {
    sine = 0,
    square,
    sawtooth,
    triangle,
}
export class RelativeParam {
    constructor(
        public time: number,
        public value: number,
    ) {
    }
}

export class RelativeParamSet extends RelativeParam {
    constructor(
        value: number,
    ) {
        super(0, value);
    }
}

export class RelativeParamTimeSet extends RelativeParam {
}

export class RelativeParamLinearRamp extends RelativeParam {
}

export class RelativeParamExponentialRamp extends RelativeParam {
}

/** A simple marker class to allow outputting to the destination device.
 */
export class Sink {
}

export const SINK = new Sink();

export abstract class AudioSlot {
    /** Upstream references, to allow cleaning up when a slot is emptied.
     */
    public upstream: Set<AudioSlot> = new Set();
    public downstream: Set<AudioSlot | Sink> = new Set();

    /** Recursively traverse the graph and get all the downstream nodes in this
     * set.
     *
     * Loops are no problem with this one.
     */
    public graph(include_self: boolean = true): Set<AudioSlot> {
        const set = new Set<AudioSlot>();
        this.add_to_set(set, include_self);
        return set;
    }

    private add_to_set(set: Set<AudioSlot>, include_self: boolean = true) {
        if (include_self) {
            set.add(this);
        }
        for (let downstream of this.downstream) {
            if (downstream instanceof AudioSlot && !set.has(downstream)) {
                downstream.add_to_set(set);
            }
        }
    }

    public abstract create(audio_context: AudioContext, start_time: number, note_length: number): AudioNode;
}

export abstract class SingleValueSlot extends AudioSlot {
    public params: Array<RelativeParam> = [];

    protected set_params(param: AudioParam, start_time: number, note_length: number) {
        for (let relative_param of this.params) {
            let canonical_time: number;
            // If positive or non-negative zero
            if (relative_param.time > 0 || Object.is(relative_param.time, 0)) {
                canonical_time = start_time + relative_param.time;
            } else {
                if (note_length <= 0) {
                    // Can't calculate from end of infinite note
                    continue;
                }
                canonical_time = start_time + note_length + relative_param.time;
            }
            if (relative_param instanceof RelativeParamSet) {
                param.value = relative_param.value;
            } else if (relative_param instanceof RelativeParamTimeSet) {
                param.setValueAtTime(relative_param.value, canonical_time);
            } else if (relative_param instanceof RelativeParamLinearRamp) {
                param.linearRampToValueAtTime(relative_param.value, canonical_time);
            } else if (relative_param instanceof RelativeParamExponentialRamp) {
                param.exponentialRampToValueAtTime(relative_param.value, canonical_time);
            }
        }
    }
}

export class GainSlot extends SingleValueSlot {
    public create(audio_context: AudioContext, start_time: number, note_length: number): GainNode {
        const gain = audio_context.createGain();
        this.set_params(gain.gain, start_time, note_length);
        return gain;
    }
}

export class DelaySlot extends SingleValueSlot {
    constructor(
        private max_seconds: number,
    ) {
        super();
    }

    public create(audio_context: AudioContext, start_time: number, note_length: number): DelayNode {
        const delay = audio_context.createDelay(this.max_seconds);
        this.set_params(delay.delayTime, start_time, note_length);
        return delay;
    }
}

export class SourceTree {
    public slot_map: Map<AudioSlot, AudioNode> = new Map();

    constructor(public source: AudioScheduledSourceNode) {
    }
}

export type AudioSourceSlot = {
    delay: number,
    length: number,
};

export class OscillatorSlot extends SingleValueSlot {
    public delay: number = 0;
    public length: number = 0;
    public tree: SourceTree | null = null;

    public constructor(
        public type: keyof typeof OscillatorType = 'sine',
    ) {
        super();
    }

    public create(audio_context: AudioContext, start_time: number, note_length: number): OscillatorNode {
        const oscillator = audio_context.createOscillator();
        oscillator.type = this.type;
        this.set_params(oscillator.frequency, start_time, note_length);
        return oscillator;
    }

    public play(audio_context: AudioContext) {
        const start_time = audio_context.currentTime + this.delay;
        const source = this.create(audio_context, start_time, this.length);
        const tree = new SourceTree(source);
        this.tree = tree;
        tree.slot_map.set(this, source);
        // Fill slot map
        for (let slot of this.graph(false)) {
            tree.slot_map.set(slot, slot.create(audio_context, start_time, this.length));
        }
        // Connect all parts
        for (const [slot, node] of tree.slot_map.entries()) {
            for (const downstream of slot.downstream) {
                if (downstream instanceof AudioSlot) {
                    const downstream_node = tree.slot_map.get(downstream)!;
                    node.connect(downstream_node);
                } else if (downstream instanceof Sink) {
                    node.connect(audio_context.destination);
                }
            }
        }
        tree.source.start(start_time);

        if (this.length > 0) {
            tree.source.stop(start_time + this.length);
        }
    }

    public stop() {
        this.tree?.source?.stop();
    }
}
