import {Console} from '/qwac.js';

async function setup() {
    const canvas = document.getElementById('screen') as HTMLCanvasElement;
    const file = document.getElementById('cartridge') as HTMLInputElement;
    const fullscreen_button = document.getElementById('fullscreen-button') as HTMLButtonElement;
    let console: Console | undefined = undefined;
    let animation_frame_id = 0;
    file.focus();

    window.addEventListener('beforeunload', function() {
        if (console !== undefined) {
            console.shutdown();
            console = undefined;
        }
    }, {capture: true, passive: true});

    file.addEventListener("change", event => {
        try {
            const files = (event.target as HTMLInputElement).files!;
            if (files.length > 0) {
                const fileReader = new FileReader();
                fileReader.onload = async event => {
                    if (console !== undefined) {
                        console.shutdown();
                        console = undefined;
                        window.cancelAnimationFrame(animation_frame_id);
                    }
                    const cartridge_data = event.target!.result as ArrayBuffer;
                    const AudioContext = window.AudioContext || (window as any).webkitAudioContext;
                    const audio_context = new AudioContext();
                    console = await Console.create(cartridge_data, canvas, audio_context);
                    async function update(timestamp: DOMHighResTimeStamp) {
                        if (console !== undefined) {
                            await console.update(timestamp);

                            animation_frame_id = window.requestAnimationFrame(update);
                        }
                    }
                    animation_frame_id = window.requestAnimationFrame(update);
                };
                fileReader.readAsArrayBuffer(files[0]);
                canvas.focus();
            }
        } catch (e) {
            alert(e);
        }
    });

    fullscreen_button.addEventListener('click', async function() {
        await canvas.requestFullscreen({navigationUI: 'hide'});
        canvas.focus();

    }, {capture: true, passive: true});

    document.addEventListener('keydown', async function(event) {
        if (event.key === 'Escape' && document.fullscreenElement) {
            await document.exitFullscreen();
            canvas.focus();
        }
    }, {capture: true, passive: true});
}

window.addEventListener('DOMContentLoaded', setup);
